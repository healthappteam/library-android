package com.bcnhealthapp.libraryhealthapp.Utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;

public class UtilPhotoManagement {

    private static final String TAG_APP = "myapp";
    private static final String TAG = "UtilPhotoManagement: ";
    private static String folderPath;

    public static String getFolderPath(Context context,String folderName) {
        if (isSdPresent()) {
            try {
                File sdPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/"+folderName);
                if(!sdPath.exists()) {
                    sdPath.mkdirs();
                    folderPath = sdPath.getAbsolutePath();
                } else if (sdPath.exists()) {
                    folderPath = sdPath.getAbsolutePath();
                }
            }
            catch (Exception e) {
                Log.d(TAG_APP, TAG + "Error al getFolderPath (isSDPresent): "+Log.getStackTraceString(e));
                UtilDebugIO.setLogDB(context, "ERROR PHOTO", "Error al getFolderPath (isSDPresent)", "PHOTO", AppConstants.ERROR);
            }
            folderPath = Environment.getExternalStorageDirectory().getPath()+"/"+folderName+"/";
        }
        else {
            try {
                File cacheDir=new File(context.getCacheDir(),folderName+"/");
                if(!cacheDir.exists()) {
                    cacheDir.mkdirs();
                    folderPath = cacheDir.getAbsolutePath();
                } else if (cacheDir.exists()) {
                    folderPath = cacheDir.getAbsolutePath();
                }
            }
            catch (Exception e){
                UtilDebugIO.setLogDB(context, "ERROR PHOTO", "Error al getFolderPath (!isSDPresent)", "PHOTO", AppConstants.ERROR);
            }
        }
        return folderPath;
    }

    public static boolean isSdPresent() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * @param url_photo parameter of WS.
     * @return true if parameter is a http link.
     */
    private static boolean _isPhoto(String url_photo) {

        String start = "http";
        String[] http = url_photo.split("://");
        return http[0].equals(start);
    }
}
