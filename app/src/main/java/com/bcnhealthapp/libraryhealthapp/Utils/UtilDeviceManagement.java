package com.bcnhealthapp.libraryhealthapp.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;


/**
 * Created by Usuari on 26/11/2015.
 */
public class UtilDeviceManagement {

    private static String TAG= "UtilDeviceManagement";

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release +")";
    }

    /**
     *
     * @return heightInPixels
     */
    public static int getScreenHeightInPixels() {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        int heightInPixels = 0;
        try{
            heightInPixels = metrics.heightPixels;
        }catch(Exception e){
            Log.d(TAG, "error al calcular la altura en pixels");
        }
        if(heightInPixels <= 0)Log.d(TAG, "No puede ser al altura de un movil 0!");

        return heightInPixels;
    }
    /**
     * @return widthInPixels
     */
    public static int getScreenWidthInPixels() {
        int widthInPixels = 0;
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        try{
            widthInPixels = metrics.widthPixels;
        }catch(Exception e){
            Log.d(TAG, "error al calcular la anchura en pixels");
        }
        if(widthInPixels <= 0) Log.d(TAG, "No puede ser al anchura de un movil 0!");

        return widthInPixels;
    }
    /**
     *
     * @return Densidad de pantalla
     */
    public static String getScreenDensityOfDevice(){
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        switch(metrics.densityDpi){
            case DisplayMetrics.DENSITY_LOW:
                return AppConstants.LDPI;
            case DisplayMetrics.DENSITY_MEDIUM:
                return AppConstants.MDPI;
            case DisplayMetrics.DENSITY_HIGH:
                return AppConstants.HDPI;
            case DisplayMetrics.DENSITY_XHIGH:
                return AppConstants.XHDPI;


        }
        return AppConstants.XHDPI;
    }

    /**
     *retorna la altura de el movil en dps
     * @param context
     * @return
     */
    public static float getDpHeight(Context context){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return  (displayMetrics.heightPixels / displayMetrics.density);
    }
    /**
     * Retorna la anchura del movil en dps
     * @param context
     * @return
     */
    public static float getDpWidth(Context context){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return  (displayMetrics.widthPixels / displayMetrics.density);
    }

}
