package com.bcnhealthapp.libraryhealthapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Usuari on 07/03/2016.
 */
public class UtilActivityManagement {

    private static final String TAG = "UtilActivityManagement";

    /**
     * Por defecto el path de base sera el de:
     * com.app.health.tcappfinalproject.UI.View.  (esta en appConstants)
     * porque expressa la direccion donde estan las vistas que son los
     * que hay que abrir con los intents
     * @param context
     * @param activityName
     */
    public static void openActivityByString(Context context,String activityName){
        try {
            activityName = activityName.replaceAll("\\s+","");
            Intent openNewIntent = new Intent(context, Class.forName( activityName ) );
            context.startActivity(openNewIntent);
        } catch (ClassNotFoundException e) {
            UtilDebugIO.setLogDB(context, "Fallo en la navegación de la app", Log.getStackTraceString(e), "TODOS", AppConstants.ERROR);
            Log.d(TAG, "Error al openActivityByString: "+Log.getStackTraceString(e));
        }
    }
    /**
     * Por defecto el path de base sera el de:
     * com.app.health.tcappfinalproject.UI.View.  (esta en appConstants)
     * porque expressa la direccion donde estan las vistas que son los
     * que hay que abrir con los intents
     * @param context
     * @param activityName
     */
    public static Intent getActivityByString(Context context,String activityName){
        Intent openNewIntent = null;
        try {
            openNewIntent = new Intent( context, Class.forName( activityName ) );
            openNewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            Log.d(TAG, "error al openActivityByString");
            e.printStackTrace();
        }
        return openNewIntent;
    }
}
