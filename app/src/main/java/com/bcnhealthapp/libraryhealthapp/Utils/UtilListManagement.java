package com.bcnhealthapp.libraryhealthapp.Utils;


import java.util.List;

public class UtilListManagement {

    private static final String TAG = "UtilListManagement";

    public static boolean existInArrayList(List<String> list,String name){
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).equals(name)){
                return true;
            }
        }
        return false;
    }
}
