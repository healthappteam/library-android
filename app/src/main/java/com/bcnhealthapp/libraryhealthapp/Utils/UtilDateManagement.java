package com.bcnhealthapp.libraryhealthapp.Utils;


import android.util.Log;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Clase para el manejo de fechas
 * Cuidado que la funcion getMes enero es el mes 0 , febrero el 1 ...!!
 *
 */
public class UtilDateManagement {

    private static final String TAG ="UtilDateManagement";

    public static int getDay(){
        Calendar rightNow = Calendar.getInstance();
        int dayOfMonth = rightNow.get(Calendar.DAY_OF_MONTH);
        return dayOfMonth;
    }
    public static int getMonth(){
        Calendar rightNow = Calendar.getInstance();
        int month = rightNow.get(Calendar.MONTH); // Jan = 0, dec = 11
        return month;
    }
    public static int getYear(){
        Calendar rightNow = Calendar.getInstance();
        int year = rightNow.get(Calendar.YEAR);
        return year;
    }
    public static int getHour(){
        //0 a 24 horas
        Calendar rightNow = Calendar.getInstance();
        int hourOfDay = rightNow.get(Calendar.HOUR_OF_DAY);
        return hourOfDay;
    }
    public static int getMinute(){
        Calendar rightNow = Calendar.getInstance();
        int minute = rightNow.get(Calendar.MINUTE);
        return minute;
    }
    public static int getSeconds(){
        Calendar rightNow = Calendar.getInstance();
        int minute = rightNow.get(Calendar.SECOND);
        return minute;
    }
    /**
     * Esta funcion calcula en que franja del dia estamos.
     * Se puede modificar las horas si no se esta de acuerdo con
     * los intervalos
     * @return franja horaria
     */
    public static String obtenerFranjaHorariaDia(){
        int hour = UtilDateManagement.getHour();
        if(hour >= 8 && hour < 15)return AppConstants.MORNING;
        if(hour >=15 && hour < 20)return AppConstants.AFTERNOON;
        else return AppConstants.NIGHT;
    }

    public static String getTimeInStringFormat(){

        Date date=new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String formattedDate = formatter.format(date);
        return formattedDate;
    }
    public static int getDay(String date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date theDate = null;
        try {
            theDate = format.parse(date);
        } catch (Exception e) {
            Log.d(TAG, "error en el getDay(): " + Log.getStackTraceString(e));
        }
        Calendar calendario = new GregorianCalendar();
        calendario.setTime(theDate);
        return calendario.get(Calendar.DAY_OF_MONTH);

    }
    public static Timestamp  getTimeStampFromString(String dateString){
        DateFormat formatter;
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date;
        java.sql.Timestamp timeStampDate = null;
        try {
            date =  formatter.parse(dateString);
            timeStampDate = new Timestamp(date.getTime());
        } catch (Exception e) {
            Log.d(TAG, "error en el getTimeStampFromString(): " + Log.getStackTraceString(e));
        }
        return timeStampDate;
    }
    public static long convertToTimeStamp(String date,String formatPattern){
        return 0;
    }

    /**
     * retorna la fecha para guardar la imagen en el servidor con la fecha del
     * momento de hacerla
     * @return
     */
    public static String getTimeInStringFormatForPhotoServer(){

        Date date=new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-ddHH:mm:ss");

        String formattedDate = formatter.format(date);
        return formattedDate;
    }

    /**
     * Funcion para escribir la hora en el formato deseado
     * al guardar una row en la tabla EVENTS
     * @return
     */
    public static String getTimeInStringFormatEvent(){
        Date date=new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        String formattedDate = formatter.format(date);
        return formattedDate;
    }

    /**
     * Se le pasa la fecha y el patron en la que se quiere convertir ejemplo
     * yyyy-MM-dd tiene que ser el mismo patron que la entrada
     * @param dataInString
     * @return null or date
     */
    public static Calendar getDataFromStringDateInFormat(String dataInString) {

        Calendar calendar = null;

        try{

            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
            Date dateObj = curFormater.parse(dataInString);
            calendar = Calendar.getInstance();
            calendar.setTime(dateObj);

            //calendar = formatter.parse(dataInString);
            //Log.d(TAG, "dataaaa" + calendar);
            //Log.d(TAG, "dataaaa" + date.getDay());

         } catch (Exception e) {
              Log.d(TAG, "Catch getDataFromStringDateInFormat: " + Log.getStackTraceString(e));
         }
        return calendar;
    }
    public static long daysBetween(Calendar startDate, Calendar endDate) {
        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(end - start);
    }

    public static long getTimeWithoutHours(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND , 0);
        cal.set(Calendar.MINUTE , 0);
        cal.set(Calendar.HOUR_OF_DAY , 0);

        return cal.getTimeInMillis()/1000;
    }

    /**
     * Esta funcion servira para obtener la fecha que se la pasara al ws
     * getUserTraking que ese formato de fecha sera por ejemplo
     * 21/07/2016 dd/mm/yyyy
     * @return
     */
    public static String getDateInTrackingFormat(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        return date;
    }
    public static String getDateInTrackingFormatUI(String dateString) {
        Date date = null;
        DateFormat to = null;
        DateFormat from = null;
        String finallDate = "";
        try{
             to   = new SimpleDateFormat("dd/MM"); // wanted format
             from = new SimpleDateFormat("yyyy-MM-dd"); // current format
            finallDate = to.format(from.parse(dateString));
        }catch (Exception e) {
            Log.d(TAG, "error en el getDateInTrackingFormatUI: " + Log.getStackTraceString(e));
        }finally{
            return finallDate ;
        }
    }

    public static String getDateInTrackingSectionFormatUI(String dateString) {
        String end = "\\.0";
        String[] date = dateString.split(end);
        DateFormat to;
        DateFormat from;
        String finallDate = "";
        try{
            to   = new SimpleDateFormat("HH:mm"); // wanted format
            from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // current format
            finallDate = to.format(from.parse(date[0]));
        }catch (Exception e) {
            Log.d(TAG, "error en el getDateInTrackingFormatUI: " + Log.getStackTraceString(e));
        }finally{
            return finallDate ;
        }
    }

}