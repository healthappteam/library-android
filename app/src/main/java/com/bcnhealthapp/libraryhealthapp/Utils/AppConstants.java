package com.bcnhealthapp.libraryhealthapp.Utils;

/**
 * En esta clase iran todas la variables globales estaticas de la aplicacion
 */
public class AppConstants {

    //    ================================ PARA LOS SKINS =================================

    public static String APP_VERSION = "1";
    public static boolean IS_DEBUG = false;
    public static int BIG_NUMBER = 1000000;


    public static String TEEN = "TEEN";
    public static String ADULT = "ADULT";
    public static String SKIN_TYPE = AppConstants.TEEN;

    public static String STANDARD = "STANDARD";
    public static String CHRISTMAS = "CHRISTMAS";
    public static String SKIN_TIME = AppConstants.STANDARD;

    //    ================================ PARA LAS DENSIDADES DE PANTALLA =================================
    public final static String LDPI ="LDPI";
    public final static String MDPI ="MDPI";
    public final static String HDPI ="HDPI";
    public final static String XHDPI ="XHDPI";
    public final static String XXHDPI ="XXHDPI";

    //    ================================ PARA LA APP GENERAL=================================
    public static String CORRECT = "CORRECT";
    public static String INCORRECT = "INCORRECT";
    public static String ACTIVE = "ACTIVE";
    public static String INACTIVE = "INACTIVE";
    public static String USER_NOT_FOUND = "NOT_FOUND";
    public static String appLanguage = "ES";
    public static int userSex = 1;// 0 si masculino 1 si femenino se cargara de la base de datos en splashActivity
    public static String usr_icode = "";//solo hay un usuario y al iniciar la app se cargara de la bd

    //    PREFIXES LOGS
    public static String LIFE_CYCLE = "@@";
    public static String NETWORK_VOLLEY = "##";
    public static String NETWORK_HTTP_DEPRECATED ="$$";
    public static String TESTER ="%%";
    public static String QUERY ="&&";
    public static String DIARYTEST ="//%";
    public static String DAOTEST =">>>>";



    /*
     sec_icode de las diferentes secciones de la aplicacion
     vienen determinados por el BO y cualquier cambio en el id de una seccion
     del BO hay que cambiarlo aquÃ­
     */
    public static final int sec_icode_of_Emotions = 1;
    public static final int sec_icode_of_Meals = 2;
    public static final int sec_icode_of_Diary = 3;
    //    public static final int QUE_ICODE = 2;//ita
    public static int QUE_ESTANDARD = 1;
    public static int QUE_ITA = 2;
    public static int QUE_SETCA = 3;
    public static int QUE_ICODE = QUE_ESTANDARD;

    //Constantes de franja horaria
    public final static String MORNING = "MORNING";
    public final static String AFTERNOON = "AFTERNOON";
    public final static String NIGHT = "NIGHT";

    //Sections
    public final static String EMOTIONS = "EMOTIONS";
    public final static String DIARY = "DIARY";
    public final static String MEALS = "MEALS";
    public final static String AWARDS = "AWARDS";
    public final static String RANKING = "RANKING";
    public final static String LOGIN = "LOGIN";
    public final static String MESSAGES = "MESSAGES";

    //cargar subpantalla emociones
    public static String EMOTIONS_SLIDER_FEMALE_ICON = SKIN_TYPE+"_EMOTIONS_SLIDER_FEMALE_ICON";
    public static String EMOTIONS_SLIDER_MALE_ICON = SKIN_TYPE+"_EMOTIONS_SLIDER_MALE_ICON";

    //cargar diario
    public static String DIARY_SITUATION_ICON = SKIN_TYPE+"_DIARY_SITUATION_ICON";

    public static String ICONDIARYMS = SKIN_TYPE+"_DIARY_MS_ICON";
    public static String ICONDIARYMSOK = SKIN_TYPE+"_ICONDIARYMSOK";


    public final static String SITUATION = "SITUATION";
    public final static String VOMITED = "VOMITED";
    public final static String RESTRICTION = "RESTRICTION";
    public final static String BINGES = "BINGES";
    public final static String TELLME = "TELLME";
    public final static String THINKING = "THINKING";
    public final static String SPORT = "SPORT";
    public final static String LAXATIVE = "LAXATIVE";
    public final static String SNACKED = "SNACKED";

    public final static String OTHER = "OTHER";

    public final static int ITA_ICODE_OTHERSITUATION = 32;
    public final static int ATT_ICODE_OTHERSITUATION = 6;
    public final static String OTHERSITUATION = "OTHERSITUATION";

    public final static int ITA_ICODE_OTHERSPORT = 125;
    public final static int ATT_ICODE_OTHERSPORT = 51;
    public final static String OTHERSPORT = "OTHERSPORT";

    public final static String JSON_QUESTIONAIRE = "JSON_QUESTIONAIRE";
    public final static String QUESTIONNAIRE = "QUESTIONNAIRE";



    //cargar subpantalla MEALS
    public static String MEALS_COMPANY_ICON = SKIN_TYPE+"_MEALS_COMPANY_ICON";
    public static String MEALS_QUANTITY_ICON = SKIN_TYPE+"_MEALS_QUANTITY_ICON";
    public static String MEALS_TIME_ICON = SKIN_TYPE+"_MEALS_TIME_ICON";
    public static String MEALS_WHERE_ICON = SKIN_TYPE+"_MEALS_WHERE_ICON";
    public static String MEALS_HOWFEEL_MALE_ICON = SKIN_TYPE+"_MEALS_HOWFEEL_MALE_ICON";
    public static String MEALS_HOWFEEL_FEMALE_ICON = SKIN_TYPE+"_MEALS_HOWFEEL_FEMALE_ICON";
    public static String MEALS_COMPANY_PARENTS_ICON = SKIN_TYPE+"_MEALS_COMPANY_PARENTS_ICON";
    //para cargar Emotions
    public static String EMOTIONS_FEMALE_ICON =  SKIN_TYPE+"_EMOTIONS_FEMALE_ICON";
    public static String EMOTIONS_MALE_ICON =  SKIN_TYPE+"_EMOTIONS_MALE_ICON";

    public static void updateSkinConstants(){
        MEALS_COMPANY_ICON = SKIN_TYPE+"_MEALS_COMPANY_ICON";
        MEALS_QUANTITY_ICON = SKIN_TYPE+"_MEALS_QUANTITY_ICON";
        MEALS_TIME_ICON = SKIN_TYPE+"_MEALS_TIME_ICON";
        MEALS_WHERE_ICON = SKIN_TYPE+"_MEALS_WHERE_ICON";
        MEALS_HOWFEEL_MALE_ICON = SKIN_TYPE+"_MEALS_HOWFEEL_MALE_ICON";
        MEALS_HOWFEEL_FEMALE_ICON = SKIN_TYPE+"_MEALS_HOWFEEL_FEMALE_ICON";
        MEALS_COMPANY_PARENTS_ICON = SKIN_TYPE+"_MEALS_COMPANY_PARENTS_ICON";
        EMOTIONS_FEMALE_ICON =  SKIN_TYPE+"_EMOTIONS_FEMALE_ICON";
        EMOTIONS_MALE_ICON =  SKIN_TYPE+"_EMOTIONS_MALE_ICON";
        DIARY_SITUATION_ICON = SKIN_TYPE+"_DIARY_SITUATION_ICON";
        ICONDIARYMS = SKIN_TYPE+"_DIARY_MS_ICON";
        ICONDIARYMSOK = SKIN_TYPE+"_ICONDIARYMSOK";
        EMOTIONS_SLIDER_FEMALE_ICON = SKIN_TYPE+"_EMOTIONS_SLIDER_FEMALE_ICON";
        EMOTIONS_SLIDER_MALE_ICON = SKIN_TYPE+"_EMOTIONS_SLIDER_MALE_ICON";
    }

    //Para pantalla AWARDS
    public final static String TODO = "TODO";
    public final static String INPROCESS = "INPROCESS";
    public final static String COMPLETED = "COMPLETED";
    public final static int NUM_OF_AWARDS = 18;

    //Mirar userSex
    public final static int FEMALE = 1;
    public final static int MALE = 0;

    //Mirar userLanguage
    public final static String SPAIN = "ES";
    public final static String ENGLAND = "EN";
    public final static String CATALONIA = "CA";
    public final static String FRANCE = "FR";

    //Para los tipos de LOGS
    public final static String ERROR = "ERROR";
    public final static String DEBUG = "DEBUG";

    //para saber la conexion a internet----
    public static String WIFI = "Hay conexion WIFI";
    public static String MOBILE_3G = "Hay conexion 3G";
    public static String NO_CONNECTION = "Hay conexion";
    public final static  int MAX_LENGTH_VAR_ICODE = 180;

    //-------------------- PARA LOS WS ------------------------------

    //============================//
    //          NEXICA            //
    //============================//

//    public static final String SERVICE_URL_WS_LOGIN = "https://212.92.57.17:8080/HealthApp/rest/wsResources/Login";
//    public static final String SERVICE_URL_WS_UPLOAD_RESPONSE = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setResponse";
//    public static final String SERVICE_URL_WS_UPLOAD_EVENT = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setEvents";
//    public static final String SERVICE_URL_WS_DOWNLOAD_MESSAGES = "https://212.92.57.17:8080/HealthApp/rest/wsResources/getUserMessages";
//    public static final String SERVICE_URL_WS_UPLOAD_MESSAGES = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setMessageRead";
//    public static final String SERVICE_URL_WS_USER_STATUS = "https://212.92.57.17:8080/HealthApp/rest/wsResources/getUserStatus?id=";
//    public static final String SERVICE_URL_WS_UPLOAD_PREFERENCES = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setUserPrefs";
//    public static final String SERVICE_URL_WS_UPLOAD_LOGS = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setLog";
//    public static final String SERVICE_URL_WS_UPLOAD_AWARDS = "https://212.92.57.17:8080/HealthApp/rest/wsResources/setAwardValue";


    //------ New appConstans NEWMODEL -----------------------

    public static final String NONE = "NONE";
    public static final int ID_ROOT = -1;


    //============================//
    //          AZURE             //
    //============================//

    public static final String SERVICE_URL_WS_LOGIN = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/Login";
    public static final String SERVICE_URL_WS_UPLOAD_RESPONSE = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setResponse";
    public static final String SERVICE_URL_WS_UPLOAD_EVENT = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setEvents";
    public static final String SERVICE_URL_WS_DOWNLOAD_MESSAGES = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getPendingMessages";
    public static final String SERVICE_URL_WS_GET_MESSAGES = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getMessages";
    public static final String SERVICE_URL_WS_UPLOAD_MESSAGES = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setMessageRead";
    public static final String SERVICE_URL_WS_USER_STATUS = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserStatus?id=";
    public static final String SERVICE_URL_WS_LATEST_VERSION = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/isLatestVersion?version=";
    public static final String SERVICE_URL_WS_UPLOAD_PREFERENCES = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setUserPrefs";
    public static final String SERVICE_URL_WS_UPLOAD_LOGS = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setLog";
    public static final String SERVICE_URL_WS_UPLOAD_AWARDS = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setAwardValue";
    public static final String SERVICE_URL_WS_DOWNLOAD_AWARDS = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserAwards";
    public static final String SERVICE_URL_WS_USER_GET_QUESTIONNAIRE = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getJsonQuestionnaire?id=";
    public static final String SERVICE_URL_WS_USER_SET_SCORE = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setUserScore";
    public static final String SERVICE_URL_WS_USER_GET_RANKING = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserRanking";
    public static final String SERVICE_URL_WS_FACE_GET_USER_ID = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserID";
    public static final String SERVICE_URL_WS_GET_USER_TRACKING = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserTracking";
    public static final String SERVICE_URL_WS_SET_USER_MESSAGE = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setUserMessage";
    public static final String SERVICE_URL_WS_GET_USER_TRACKING_DETAILS = "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/getUserTrackingDetails";
    public static final String SERVICE_URL_WS_SET_USER_DEVICE= "https://tcapp.bcnhealthapp.com/HealthApp/rest/wsResources/setUserDevice";

    //============================//
    //          LOCAL             //
    //============================//
//
//    public static final String SERVICE_URL_WS_LOGIN = "http://172.16.151.84:8080/HealthApp/rest/wsResources/Login";
//    public static final String SERVICE_URL_WS_UPLOAD_RESPONSE = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setResponse";
//    public static final String SERVICE_URL_WS_UPLOAD_EVENT = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setEvents";
//    public static final String SERVICE_URL_WS_DOWNLOAD_MESSAGES = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getPendingMessages";
//    public static final String SERVICE_URL_WS_GET_MESSAGES = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getMessages";
//    public static final String SERVICE_URL_WS_UPLOAD_MESSAGES = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setMessageRead";
//    public static final String SERVICE_URL_WS_USER_STATUS = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserStatus?id=";
//    public static final String SERVICE_URL_WS_LATEST_VERSION = "http://172.16.151.84:8080/HealthApp/rest/wsResources/isLatestVersion?version=";
//    public static final String SERVICE_URL_WS_UPLOAD_PREFERENCES = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setUserPrefs";
//    public static final String SERVICE_URL_WS_UPLOAD_LOGS = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setLog";
//    public static final String SERVICE_URL_WS_UPLOAD_AWARDS = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setAwardValue";
//    public static final String SERVICE_URL_WS_DOWNLOAD_AWARDS = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserAwards";
//    public static final String SERVICE_URL_WS_USER_GET_QUESTIONNAIRE = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getJsonQuestionnaire?id=";
//    public static final String SERVICE_URL_WS_USER_SET_SCORE = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setUserScore";
//    public static final String SERVICE_URL_WS_USER_GET_RANKING = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserRanking";
//    public static final String SERVICE_URL_WS_FACE_GET_USER_ID = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserID";
//    public static final String SERVICE_URL_WS_GET_USER_TRACKING = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserTracking";
//    public static final String SERVICE_URL_WS_SET_USER_MESSAGE = "http://172.16.151.84:8080/HealthApp/rest/wsResources/setUserMessage";
//    public static final String SERVICE_URL_WS_GET_USER_TRACKING_DETAILS = "http://172.16.151.84:8080/HealthApp/rest/wsResources/getUserTrackingDetails";
    public static final String SERVICE_URL_WS_UPLOAD_ANSWERS = "http://192.168.1.43:8888/WS_PHP_TCAPP/ws.php";

    public static final String LOCAL_WS_PHOTO_PATH = "http://192.168.1.39:8888/scripts/UploadPhotoServer.php";


    public static final String EVENT_FLAG= "EVENT";
    public static final String RESPONSE_FLAG= "RESPONSE";
    public static final String MESSAGE_FLAG= "MESSAGE";
    public static final String AWARD_FLAG= "AWARD";
    public static final String TRACKING_FLAG= "TRACKING";


    //---------------------- PARA EL SERVICE -------------------------
    public static  int TEN_SECONDS = (1000*10);

    public static  int TWENTY_SECONDS = (1000*20);
    public static  int THIRTY_SECONDS = (1000*30);

    public static  int ONE_MINUTE = (1000*60);
    public static  int TWO_MINUTES = (1000*60*2);
    public static  int FIVE_MINUTES = (1000*60*5);
    public static  int THREE_MINUTES = (1000*60*3);

    public static  int SIX_HOURS = (1000*60*60*6);
    public static  int SEVEN_DAYS = (1000*60*60*24*7);
    public static  int THREE_DAYS = (1000*60*60*24*3);


    public static boolean isServiceACTIVE = false;

    public static  int INITIAL_PERIOD_SINCRONIZATION_RESPONSES = FIVE_MINUTES;
    public static  int INITIAL_PERIOD_UPLOAD_MESSAGES = THREE_MINUTES;
    public static  int INITIAL_PERIOD_DOWNLOAD_MESSAGES = FIVE_MINUTES;
    public static  int INITIAL_PERIOD_UPDATE_DATABASE = SEVEN_DAYS;

    //---------------------------- PARA SUBIR FOTO AL SERVER -------------------------

//    public static String URL_UPLOAD_PHOTO_SERVER = "http://212.92.57.17/upfotos/wsuploadfoto.php";
//    public static String URL_PHOTO_SERVER = "http://212.92.57.17/upfotos/";

    public static String URL_UPLOAD_PHOTO_SERVER = "http://mysql-healthapp.westeurope.cloudapp.azure.com/tcapictures/uploadPhoto.php";
    public static String URL_PHOTO_SERVER = "http://mysql-healthapp.westeurope.cloudapp.azure.com/tcapictures/";
}